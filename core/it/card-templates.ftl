card-templates-changes-will-affect-notes =
    { $count ->
        [one] I cambiamenti qui sotto avranno effetto su { $count } nota che usa questo tipo di carta.
       *[other] I cambiamenti qui sotto avranno effetto su { $count } note che usano questo tipo di carta.
    }
card-templates-card-type = Tipo di carta:
card-templates-front-template = Modello fronte
card-templates-back-template = Modello retro
card-templates-template-styling = Stile
card-templates-front-preview = Anteprima fronte
card-templates-back-preview = Anteprima retro
card-templates-preview-box = Anteprima
card-templates-template-box = Modello
card-templates-sample-cloze = Questo è un {"{{c1::"}esempio{"}}"} di testo da completare.
