empty-cards-window-title = Carte vuote
empty-cards-delete-button = Cancella
empty-cards-not-found = Nessuna carta vuota.
empty-cards-deleted-count =
    Cancellate { $cards ->
        [one] { $cards } carta.
       *[other] { $cards } carte.
    }
