card-templates-changes-will-affect-notes =
    { $count ->
        [one] Unten vorgenommene Änderungen werden { $count } Notiz betreffen, die diesen Kartentyp verwenden.
       *[other] Unten vorgenommene Änderungen werden { $count } Notizen betreffen, die diesen Kartentyp verwenden.
    }
card-templates-card-type = Kartentyp:
card-templates-front-template = Vorlage für Vorderseite
card-templates-back-template = Vorlage für Rückseite
card-templates-template-styling = Stil
card-templates-front-preview = Vorschau für Vorderseite
card-templates-back-preview = Vorschau für Rückseite
card-templates-preview-box = Vorschau
card-templates-template-box = Vorlage
card-templates-sample-cloze = Dies ist ein { "{{c1::" }Beispiel{ "}}" } - Lückentext.
card-templates-fill-empty = Leere Felder ausfüllen
