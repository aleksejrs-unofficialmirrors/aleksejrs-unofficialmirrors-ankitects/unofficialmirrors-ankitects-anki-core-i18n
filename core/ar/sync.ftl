sync-media-check-required = حدثت مشكلة أثناء مزامنة الوسائط. الرجاء استخدام أدوات>التحقق من الوسائط، ثم المزامنة مجددًا لحل المشكلة.
sync-wrong-pass = .معرف آنكي ويب أو كلمة السر غير صحيحة. حاول مجددًا
sync-cancel-button = إلغاء الأمر
sync-checking = ...يجري الفحص
sync-ankiweb-id-label = :معرف آنكي ويب
sync-password-label = كلمة السر:
sync-account-required =
    <h1>يلزم حساب</h1>
    { "." }حساب، ثم إدخال التفاصيل في الأسفل <a href="{ $link }">تسجيل</a> يلزم حساب مجاني لإبقاء مجموعتك متزامنة. الرجاء
sync-connecting = جاري الإتصال...
