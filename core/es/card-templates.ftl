card-templates-changes-will-affect-notes =
    { $count ->
        [one] Los cambios debajo afectarán { $count } nota que utiliza este tipo de tarjeta.
       *[other] Los cambios debajo afectarán { $count } notas que utilizan este tipo de tarjeta.
    }
card-templates-card-type = Tipo de Tarjeta:
card-templates-front-template = Plantilla del Anverso
card-templates-back-template = Plantilla del Reverso
card-templates-template-styling = Estilo
card-templates-front-preview = Vista previa del Anverso
card-templates-back-preview = Vista previa del Reverso
card-templates-preview-box = Previsualizar
card-templates-template-box = Plantilla
card-templates-sample-cloze = Esto es un  { "{{c1::" }ejemplo{ "}}" } de respuesta anidada.
card-templates-fill-empty = Rellenar campos vacíos
