findreplace-notes-updated = { $total ->
    [one] {$changed} מתוך {$total} הערה עודכנה
    [two] {$changed} מתוך {$total} הערות עודכנו
    [many] {$changed} מתוך {$total} הערות עודכנו
   *[other] {$changed} מתוך {$total} הערות עודכנו
  }
