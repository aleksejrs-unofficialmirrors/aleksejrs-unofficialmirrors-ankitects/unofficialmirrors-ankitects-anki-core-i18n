card-templates-changes-will-affect-notes =
    { $count ->
        [one] השינויים למטה ישפיעו על { $count } הערה שמשתמשת בסוג כרטיסייה זה.
        [two] השינויים למטה ישפיעו על { $count } הערות שמשתמשות בסוג כרטיסייה זה.
        [many] השינויים למטה ישפיעו על { $count } הערות שמשתמשות בסוג כרטיסייה זה.
       *[other] השינויים למטה ישפיעו על { $count } הערות שמשתמשות בסוג כרטיסייה זה.
    }
card-templates-card-type = סוג כרטיסייה:
card-templates-front-template = תבנית חזית
card-templates-front-preview = תצוגה-מקדימה חזית
