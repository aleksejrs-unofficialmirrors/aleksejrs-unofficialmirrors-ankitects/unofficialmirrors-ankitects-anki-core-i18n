## Default field names in newly created note types

notetypes-front-field = קדמי
notetypes-back-field = אחורי
notetypes-add-reverse-field = הוסף הופכי
notetypes-text-field = טקסט

## Default note type names

notetypes-basic-name = בסיסית
notetypes-basic-reversed-name = בסיסית (וכרטיסייה הופכית)
notetypes-basic-optional-reversed-name = בסיסית (כרטיסייה הופכית אופציונלית)
notetypes-basic-type-answer-name = בסיסית (הקלד את התשובה)

## Default card template names

notetypes-card-1-name = כרטיסייה 1
notetypes-card-2-name = כרטיסייה 2
