card-templates-changes-will-affect-notes =
    { $count ->
        [one] Следующие изменения затронут { $count } запись, использующую этот тип карточек.
        [few] Следующие изменения затронут { $count } записи, использующие этот тип карточек.
       *[other] Следующие изменения затронут { $count } записей, использующих этот тип карточек.
    }
card-templates-card-type = Тип карточки:
card-templates-front-template = Шаблон лицевой стороны
card-templates-back-template = Шаблон оборотной стороны
card-templates-template-styling = Таблица стилей
card-templates-front-preview = Предпросмотр лицевой стороны
card-templates-back-preview = Предпросмотр оборотной стороны
card-templates-preview-box = Предпросмотр
card-templates-template-box = Шаблон
card-templates-sample-cloze = Вот { "{{c1::" }пример{ "}}" } заполнения пропуска.
card-templates-fill-empty = Заполнить пустые поля
