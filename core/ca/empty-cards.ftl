empty-cards-delete-button = Eliminar
empty-cards-not-found = No hi ha targuetes buides.
empty-cards-deleted-count =
    { $cards ->
        [one] { $cards } tarjeta eliminada.
       *[other] { $cards } targetes eliminades.
    }
