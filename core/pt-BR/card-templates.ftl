card-templates-changes-will-affect-notes =
    { $count ->
        [one] As alterações abaixo afetarão a { $count } nota que usa este tipo de carta.
       *[other] As alterações abaixo afetarão as { $count } notas que usam este tipo de carta.
    }
card-templates-card-type = Tipo da Carta:
card-templates-front-template = Modelo da Frente
card-templates-back-template = Modelo do Verso
card-templates-template-styling = Estilo
card-templates-front-preview = Visualizar a Frente
card-templates-back-preview = Visualizar o Verso
card-templates-preview-box = Pré-visualização
card-templates-sample-cloze = Isto é uma {"{{c1::"}sample{"}}"} omissão de palavras.
