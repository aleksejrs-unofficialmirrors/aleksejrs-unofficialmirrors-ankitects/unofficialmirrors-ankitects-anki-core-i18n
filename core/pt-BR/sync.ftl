sync-media-check-required = Um problema ocorreu durante a sincronização. Por favor, vá em Ferramentas>Verificar Mídia, e sincronize novamente para corrigir o problema.
sync-wrong-pass = O Usuário AnkiWeb ou senha está incorreto; por favor, tente outra vez.
sync-confirm-empty-download = A coleção local não possui cartas. Baixar de AnkiWeb?
sync-conflict-explanation =
    Seus baralhos aqui e no AnkiWeb diferem tanto que não podem ser mesclados, então é necessário que um deles sobrescreva o outro.
    
    Se você escolher baixar, o Anki trará a coleção do AnkiWeb e todas as mudanças que você tiver feito desde a última sincronização serão perdidas.
    
    Se você escolher enviar, o Anki copiará sua coleção para o AnkiWeb e todas as mudanças que você tenha feito no AnkiWeb ou em outros aparelhos desde a última sincronização serão perdidas.
    
    Depois que todos os aparelhos estiverem sincronizados, as futuras revisões e os cartões adicionados serão mesclados automaticamente.
sync-download-from-ankiweb = Baixar do AnkiWeb
sync-upload-to-ankiweb = Enviar para o AnkiWeb
sync-cancel-button = Cancelar
sync-downloading-from-ankiweb = Baixando do AnkiWeb...
sync-uploading-to-ankiweb = Enviando para o AnkiWeb...
sync-syncing = Sincronizando...
sync-checking = Verificando...
sync-ankiweb-id-label = Usuário AnkiWeb:
sync-password-label = Senha:
sync-account-required =
    <h1>Requer conta</h1>
    Uma conta grátis é necessária para manter sua coleção sincronizada. Por favor, <a href="{ $link }">registre-se</a> e então insira detalhes abaixo.
sync-connecting = Conectando...
