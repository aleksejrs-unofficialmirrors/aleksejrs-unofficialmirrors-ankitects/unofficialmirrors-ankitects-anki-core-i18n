sync-media-check-required = 미디어를 동기화하는 도중 문제가 발생했습니다. 도구>미디어 검사를 실행한 뒤, 다시 동기화하여 이 문제를 해결하세요.
sync-wrong-pass = AnkiWeb 아이디나 비밀번호가 틀렸습니다. 다시 시도하세요.
sync-confirm-empty-download = 지금 저장된 컬렉션에는 카드가 존재하지 않습니다. Ankiweb에서 다운로드할까요?
sync-conflict-explanation =
    이 기기에 저장된 내용과 AnkiWeb에 저장된 내용이 차이가 나기 때문에, 한쪽의 내용을 다른 쪽으로 덮어써야만 합니다.
    
    AnkiWeb의 내용을 이 컴퓨터에 덮어쓰려면 'AnkiWeb에서 다운로드'를 선택해 주세요. (주의: 마지막 동기화 이후 이 컴퓨터에서 이루어진 변경 사항은 소실됩니다)
    이 컴퓨터의 내용을 AnkiWeb에 덮어쓰려면 'AnkiWeb으로 업로드'를 선택해 주세요. (주의: 마지막 동기화 이후 AnkiWeb이나 다른 기기에서 이루어진 변경 사항은 소실됩니다.)
    
    모든 기기들이 동기화되고 나면, 이후의 복습 내용과 추가된 카드는 자동적으로 처리될 수 있습니다.
sync-download-from-ankiweb = AnkiWeb에서 다운로드
sync-upload-to-ankiweb = AnkiWeb으로 업로드
sync-cancel-button = 취소
sync-downloading-from-ankiweb = AnkiWeb에서 다운로드하는 중...
sync-uploading-to-ankiweb = AnkiWeb에 업로드 중...
sync-syncing = 동기화 중...
sync-checking = 검사 중...
sync-ankiweb-id-label = AnkiWeb 아이디:
sync-password-label = 비밀번호:
sync-account-required =
    <h1>계정이 필요합니다</h1>
    무료 계정이 있어야 사용자의 모음집을 동기화할 수 있습니다. <a href="{ $link }">사용자 등록</a>을 한 뒤, 필요한 정보를 아래에 입력하세요.
sync-connecting = 연결 중...
