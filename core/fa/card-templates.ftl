card-templates-changes-will-affect-notes =
    { $count ->
       *[other] تغییرات زیر { $count } عدد یادداشت که این نوع کارت را استفاده می‌کنند تغییر خواهد داد.
    }
card-templates-card-type = نوع کارت:
card-templates-front-template = قالب جلو
card-templates-back-template = قالب پشت
card-templates-template-styling = تزئینات
card-templates-front-preview = پیش‌نمایش جلو
card-templates-back-preview = پیش‌نمایش پشت
card-templates-preview-box = پیش‌نمایش
card-templates-template-box = قالب
card-templates-sample-cloze = این یک { "{{c1::" }نمونه{ "}}" } کارت جاخالی است.
card-templates-fill-empty = پر کردن فیلدهای خالی
