card-templates-changes-will-affect-notes =
    { $count ->
       *[other] 以下變更將會影響{ $count }則使用此卡片類型的筆記。
    }
card-templates-card-type = 卡片類型：
card-templates-front-template = 正面模板
card-templates-back-template = 背面模板
card-templates-template-styling = 樣式
card-templates-front-preview = 正面預覽
card-templates-back-preview = 背面預覽
card-templates-preview-box = 預覽
card-templates-template-box = 模板
card-templates-sample-cloze = 這是一個克漏字空格{ "{{c1::" }範例{ "}}" }。
card-templates-fill-empty = 填入空白欄位
